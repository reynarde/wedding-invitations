﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InvitationSend.Models.Entity;
using InvitationSend.Helpers;

namespace InvitationSend.Models.Logic
{
    public class Guest
    {
        public static void create(guest model)
        {
            using (WunderfortDB db = new WunderfortDB())
            {
                model.sGuid = $"{Guid.NewGuid():N}";
                db.guests.Add(model);
                db.SaveChanges();
            }
        }

        public static void update(guest model)
        {
            using (WunderfortDB db = new WunderfortDB())
            {
                var entity = db.guests.Where(m => m.sGuid == model.sGuid).FirstOrDefault();

                if (entity != null)
                {
                    entity.bComming = model.bComming;
                    entity.bPartner = model.bPartner;
                    db.SaveChanges();
                }
            }
        }

        public static List<guest> Read()
        {
            WunderfortDB db = new WunderfortDB();
            var list = new List<guest>();//db.guests.ToList();
            return list;
        }
    }
}