namespace InvitationSend.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class guest
    {
        [Key]
        public long pklid { get; set; }

        public string sGuid { get; set; }

        public string sName { get; set; }

        public string sSurname { get; set; }

        public string sEmail { get; set; }

        public string sCellNumber { get; set; }

        public bool bPartner { get; set; }
        
        public bool bComming { get; set; }
    }
}
