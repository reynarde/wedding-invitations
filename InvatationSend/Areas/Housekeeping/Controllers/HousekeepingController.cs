﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InvitationSend.Models.Entity;
using InvitationSend.Models.Logic;
using InvitationSend.Helpers;

namespace InvitationSend.Areas.Housekeeping.Controllers
{
    public class HousekeepingController : Controller
    {
        // GET: Housekeeping/Housekeeping
        public ActionResult List()
        {
            return View();
        }

        public ActionResult Single()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Single(guest model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }

            Guest.create(model);
            return View();
        }

        public ActionResult SendInvite()
        {
            InviteSend.sendInvite();
            return View("List");
        }
    }
}