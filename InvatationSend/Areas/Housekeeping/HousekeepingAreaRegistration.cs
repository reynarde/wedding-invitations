﻿using System.Web.Mvc;

namespace InvitationSend.Areas.Housekeeping
{
    public class HousekeepingAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Housekeeping";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Housekeeping_default",
                "Housekeeping/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}