﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InvitationSend.Models.Entity;
using InvitationSend.Models.Logic;

namespace InvitationSend.Areas.Invitations.Controllers
{
    public class InvitationsController : Controller
    {
        // GET: Invitations/Invitations
        public ActionResult Index(string sGuid)
        {
            if(sGuid != null)
            {
                guest model = new guest();
                model.sName = "John Seed";
                model.bPartner = true;
                return View(model);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Index(guest model)
        {
            Guest.update(model);
            return View(model);
        }

        public ActionResult Ideas()
        {
            return View();
        }
    }
}