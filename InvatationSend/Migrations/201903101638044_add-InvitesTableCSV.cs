namespace InvitationSend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInvitesTableCSV : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GuestListCSV",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        Van = c.String(maxLength: 50),
                        Naam = c.String(maxLength: 50),
                        Metgesel = c.Int(),
                        Email = c.String(maxLength: 50),
                        CellNumber = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GuestListCSV");
        }
    }
}
