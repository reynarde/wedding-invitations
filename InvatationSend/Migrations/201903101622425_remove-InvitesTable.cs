namespace InvitationSend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeInvitesTable : DbMigration
    {
        public override void Up()
        {
            //DropTable("dbo.guestInvites");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.guestInvites",
                c => new
                    {
                        pklid = c.Long(nullable: false, identity: true),
                        sGuid = c.String(),
                        sName = c.String(),
                        sSurname = c.String(),
                        sEmail = c.String(),
                        sCellNumber = c.String(),
                        bPartner = c.Boolean(nullable: false),
                        sPartnerId = c.String(),
                        bComming = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.pklid);
            
        }
    }
}
