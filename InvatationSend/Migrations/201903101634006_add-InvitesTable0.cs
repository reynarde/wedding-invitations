namespace InvitationSend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInvitesTable0 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.guestInvites",
                c => new
                    {
                        pklid = c.Long(nullable: false, identity: true),
                        sGuid = c.String(),
                        sName = c.String(),
                        sSurname = c.String(),
                        sEmail = c.String(),
                        sCellNumber = c.String(),
                        bPartner = c.Boolean(nullable: false),
                        sPartnerId = c.String(),
                        bComming = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.pklid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.guestInvites");
        }
    }
}
